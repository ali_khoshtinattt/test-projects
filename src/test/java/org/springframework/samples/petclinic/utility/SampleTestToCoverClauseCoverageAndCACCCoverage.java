package org.springframework.samples.petclinic.utility;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.github.mryf323.tractatus.ClauseDefinition;
import com.github.mryf323.tractatus.ClauseCoverage;
import com.github.mryf323.tractatus.CACC;
import com.github.mryf323.tractatus.Valuation;
import com.github.mryf323.tractatus.experimental.extensions.ReportingExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(ReportingExtension.class)
@ClauseDefinition(clause = 'a', def = "Side1 <= 0")
@ClauseDefinition(clause = 'b', def = "Side2 <= 0")
@ClauseDefinition(clause = 'c', def = "Side3 <= 0")
@ClauseDefinition(clause = 'd', def = "Side1+Side2 <= Side3")
@ClauseDefinition(clause = 'e', def = "Side2+Side3 <= Side1")
@ClauseDefinition(clause = 'f', def = "Side1+Side3 <= Side2")
public class SampleTestToCoverClauseCoverageAndCACCCoverage {

	private static final Logger log = LoggerFactory.getLogger(TriTypeTest.class);

	@ClauseCoverage(
		predicate = "a + b + c",
		valuations = {
			@Valuation(clause = 'a', valuation = false),
			@Valuation(clause = 'b', valuation = false),
			@Valuation(clause = 'c', valuation = false)
		}
	)
	@ClauseCoverage(
		predicate = "d + e + f",
		valuations = {
			@Valuation(clause = 'd', valuation = false),
			@Valuation(clause = 'e', valuation = false),
			@Valuation(clause = 'f', valuation = false)
		}
	)
	@CACC(
		predicate = "a + b +c",
		majorClause = 'a',
		valuations = {
			@Valuation(clause = 'a', valuation = false),
			@Valuation(clause = 'b', valuation = false),
			@Valuation(clause = 'c', valuation = false)
		},
		predicateValue = false
	)
	@CACC(
		predicate = "a + b +c",
		majorClause = 'b',
		valuations = {
			@Valuation(clause = 'a', valuation = false),
			@Valuation(clause = 'b', valuation = false),
			@Valuation(clause = 'c', valuation = false)
		},
		predicateValue = false
	)
	@CACC(
		predicate = "a + b +c",
		majorClause = 'c',
		valuations = {
			@Valuation(clause = 'a', valuation = false),
			@Valuation(clause = 'b', valuation = false),
			@Valuation(clause = 'c', valuation = false)
		},
		predicateValue = false
	)
	@CACC(
		predicate = "e + d +f",
		majorClause = 'e',
		valuations = {
			@Valuation(clause = 'e', valuation = false),
			@Valuation(clause = 'd', valuation = false),
			@Valuation(clause = 'f', valuation = false)
		},
		predicateValue = false
	)
	@CACC(
		predicate = "e + d +f",
		majorClause = 'd',
		valuations = {
			@Valuation(clause = 'e', valuation = false),
			@Valuation(clause = 'd', valuation = false),
			@Valuation(clause = 'f', valuation = false)
		},
		predicateValue = false
	)
	@CACC(
		predicate = "e + d +f",
		majorClause = 'f',
		valuations = {
			@Valuation(clause = 'e', valuation = false),
			@Valuation(clause = 'd', valuation = false),
			@Valuation(clause = 'f', valuation = false)
		},
		predicateValue = false
	)
	@Test
	public void FirstTestForQuestionOne() {
		TriType tryType = new TriType();
		TriType.TryClass triClass;
		triClass = tryType.classifyTriangle(5,12,13);
		log.debug("triangle identified as {}", triClass);
		Assertions.assertEquals(TriType.TryClass.SCALENE, triClass);
	}


	@ClauseCoverage(
		predicate = "a + b + c",
		valuations = {
			@Valuation(clause = 'a', valuation = true),
			@Valuation(clause = 'b', valuation = false),
			@Valuation(clause = 'c', valuation = false)
		}
	)
	@CACC(
		predicate = "a + b +c",
		majorClause = 'a',
		valuations = {
			@Valuation(clause = 'a', valuation = true),
			@Valuation(clause = 'b', valuation = false),
			@Valuation(clause = 'c', valuation = false)
		},
		predicateValue = true
	)

	@Test
	public void SecondTestForQuestionOne() {
		TriType tryType = new TriType();
		TriType.TryClass triClass;
		triClass = tryType.classifyTriangle(0,1,1);
		log.debug("triangle identified as {}", triClass);
		Assertions.assertEquals(TriType.TryClass.NOT_VALID, triClass);
	}


	@ClauseCoverage(
		predicate = "a + b + c",
		valuations = {
			@Valuation(clause = 'a', valuation = false),
			@Valuation(clause = 'b', valuation = true),
			@Valuation(clause = 'c', valuation = false)
		}
	)
	@CACC(
		predicate = "a + b +c",
		majorClause = 'b',
		valuations = {
			@Valuation(clause = 'a', valuation = false),
			@Valuation(clause = 'b', valuation = true),
			@Valuation(clause = 'c', valuation = false)
		},
		predicateValue = true
	)

	@Test
	public void ThirdTestForQuestionOne() {
		TriType tryType = new TriType();
		TriType.TryClass triClass;
		triClass = tryType.classifyTriangle(1,0,1);
		log.debug("triangle identified as {}", triClass);
		Assertions.assertEquals(TriType.TryClass.NOT_VALID, triClass);
	}


	@ClauseCoverage(
		predicate = "a + b + c",
		valuations = {
			@Valuation(clause = 'a', valuation = false),
			@Valuation(clause = 'b', valuation = false),
			@Valuation(clause = 'c', valuation = true)
		}
	)
	@CACC(
		predicate = "a + b +c",
		majorClause = 'c',
		valuations = {
			@Valuation(clause = 'a', valuation = false),
			@Valuation(clause = 'b', valuation = false),
			@Valuation(clause = 'c', valuation = true)
		},
		predicateValue = true
	)

	@Test
	public void ForthTestForQuestionOne() {
		TriType tryType = new TriType();
		TriType.TryClass triClass;
		triClass = tryType.classifyTriangle(1,1,0);
		log.debug("triangle identified as {}", triClass);
		Assertions.assertEquals(TriType.TryClass.NOT_VALID, triClass);
	}


	@ClauseCoverage(
		predicate = "e + f + g",
		valuations = {
			@Valuation(clause = 'e', valuation = true),
			@Valuation(clause = 'f', valuation = false),
			@Valuation(clause = 'g', valuation = false)
		}
	)
	@CACC(
		predicate = "e + f +g",
		majorClause = 'e',
		valuations = {
			@Valuation(clause = 'e', valuation = true),
			@Valuation(clause = 'f', valuation = false),
			@Valuation(clause = 'g', valuation = false)
		},
		predicateValue = true
	)

	@Test
	public void FifthTestForQuestionOne() {
		TriType tryType = new TriType();
		TriType.TryClass triClass;
		triClass = tryType.classifyTriangle(1,2,3);
		log.debug("triangle identified as {}", triClass);
		Assertions.assertEquals(TriType.TryClass.NOT_VALID, triClass);
	}


	@ClauseCoverage(
		predicate = "e + f + g",
		valuations = {
			@Valuation(clause = 'e', valuation = false),
			@Valuation(clause = 'f', valuation = true),
			@Valuation(clause = 'g', valuation = false)
		}
	)
	@CACC(
		predicate = "e + f +g",
		majorClause = 'f',
		valuations = {
			@Valuation(clause = 'e', valuation = false),
			@Valuation(clause = 'f', valuation = true),
			@Valuation(clause = 'g', valuation = false)
		},
		predicateValue = true
	)

	@Test
	public void SixthTestForQuestionOne() {
		TriType tryType = new TriType();
		TriType.TryClass triClass;
		triClass = tryType.classifyTriangle(3,1,2);
		log.debug("triangle identified as {}", triClass);
		Assertions.assertEquals(TriType.TryClass.NOT_VALID, triClass);
	}


	@ClauseCoverage(
		predicate = "e + f + g",
		valuations = {
			@Valuation(clause = 'e', valuation = false),
			@Valuation(clause = 'f', valuation = false),
			@Valuation(clause = 'g', valuation = true)
		}
	)
	@CACC(
		predicate = "e + f +g",
		majorClause = 'g',
		valuations = {
			@Valuation(clause = 'e', valuation = false),
			@Valuation(clause = 'f', valuation = false),
			@Valuation(clause = 'g', valuation = true)
		},
		predicateValue = true
	)

	@Test
	public void SeventhTestForQuestionOne() {
		TriType tryType = new TriType();
		TriType.TryClass triClass;
		triClass = tryType.classifyTriangle(1,3,2);
		log.debug("triangle identified as {}", triClass);
		Assertions.assertEquals(TriType.TryClass.NOT_VALID, triClass);
	}
}

